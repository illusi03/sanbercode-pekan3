<!DOCTYPE html>
<html>
  <head>
    <title>Sanbercode - Daftar</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ url('register') }}" method="POST">
      @csrf
      <p>First Name :</p>
      <input type="text" name="firstName" /><br />
      <p>Last Name :</p>
      <input type="text" name="lastName" /><br />
      <p>Gender :</p>
      <input type="radio" name="gender" value="male" id="male" />
      <label for="male">Male</label><br />
      <input type="radio" name="gender" value="female" id="female" />
      <label for="female">Female</label><br />
      <p>Nationality :</p>
      <select name="nationality">
        <option value="id">Indonesia</option>
        <option value="jp">Japanese</option>
        <option value="ar">Saudi Arabia</option>
      </select>
      <p>Language Spoken :</p>
      <input type="checkbox" name="language" value="id" id="id" />
      <label for="id">Indonesia</label><br />
      <input type="checkbox" name="language" value="jp" id="jp" />
      <label for="jp">Japanese</label><br />
      <input type="checkbox" name="language" value="other" id="other" />
      <label for="other">Other</label>
      <p>Bio :</p>
      <textarea name="bio" rows="10" cols="30"></textarea><br />
      <button type="submit" name="signup" value="signup">
        Sign Up
      </button>
    </form>
  </body>
</html>
